#include <gtest/gtest.h>
#include <rfs_slam/GMM.h>

TEST (Gauss, ConstructorDefaultValue) {
    typedef Eigen::Vector3d state;
    typedef Eigen::Matrix3d covariance;
    Gauss<state, covariance> g;
    EXPECT_EQ(1.0, g.get_w());
    EXPECT_EQ(state(0, 0, 0), g.get_mu());
    EXPECT_EQ(covariance::Identity(), g.get_cov());
}

TEST (Gauss, ConstructorUserValue) {
    typedef Eigen::Vector3d state;
    typedef Eigen::Matrix3d covariance;
    state mu(0.5, 0.3, 0.9);
    covariance cov = covariance::Identity();
    Gauss<state, covariance> g(mu, cov, 0.2);
    EXPECT_EQ(0.2, g.get_w());
    EXPECT_EQ(mu, g.get_mu());
    EXPECT_EQ(cov, g.get_cov());
}

TEST (Gauss, pdf) {
    typedef Eigen::Vector3d state;
    typedef Eigen::Matrix3d covariance;
    double eps = 0.0001;
    state mu(0.00, 1.00, 0.00);
    covariance cov;
    cov << 1.00, 0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00, 1.00;
    Gauss<state, covariance> g(mu, cov);
    EXPECT_NEAR(0.0572764577, g.pdf(state(0.19, 1.40, 0.10)), eps);
    EXPECT_NEAR(0.0470819490, g.pdf(state(0.04, 1.06, 0.77)), eps);
    EXPECT_NEAR(0.0542655550, g.pdf(state(0.31, 0.68, 0.34)), eps);
    EXPECT_NEAR(0.0410499525, g.pdf(state(0.21, 1.01, 0.91)), eps);
    EXPECT_NEAR(0.0445423169, g.pdf(state(0.63, 0.60, 0.39)), eps);
    EXPECT_NEAR(0.0578145041, g.pdf(state(0.05, 1.00, 0.43)), eps);
    EXPECT_NEAR(0.0325520283, g.pdf(state(1.00, 1.31, 0.49)), eps);
    EXPECT_NEAR(0.0371156858, g.pdf(state(0.89, 0.64, 0.39)), eps);
    EXPECT_NEAR(0.0293191146, g.pdf(state(0.93, 1.42, 0.71)), eps);
    EXPECT_NEAR(0.0332528235, g.pdf(state(0.62, 0.84, 0.94)), eps);
}

TEST (GMM, PruneLowerThen) {
    typedef Eigen::Vector3d state;
    typedef Eigen::Matrix3d covariance;
    GMM<state, covariance> gmm;
    double sum(0), sum_prune(0);
    for (int i = 0; i < 10; ++i) {
        double w = 0.1 * (double) i;
        gmm.add_gaussian(state(0, 0, 0), covariance::Identity(), w);
        sum += w;
        if (!(w < 0.5))
            sum_prune += w;
    }

    typedef GMM<state, covariance>::vec_gauss_cit cit;
    cit last = gmm.get_gaussians().end() - 1;
    cit max = gmm.get_largest_weight_gauss();
    EXPECT_EQ(last->get_w(), max->get_w());
    EXPECT_EQ(last->get_cov(), max->get_cov());
    EXPECT_EQ(last->get_mu(), max->get_mu());
    EXPECT_EQ(10, gmm.size());
    EXPECT_DOUBLE_EQ(sum, gmm.sum());
    gmm.prune(-1, 0.5, 1000);
    EXPECT_EQ(5, gmm.size());
    EXPECT_DOUBLE_EQ(sum_prune, gmm.sum());

    gmm.prune(-1, -1, 2);
    EXPECT_EQ(2, gmm.size());
    EXPECT_DOUBLE_EQ(0.9 + 0.8, gmm.sum());
}

TEST (GMM, MergeGaussians) {
    typedef Eigen::Vector3d state;
    typedef Eigen::Matrix3d covariance;
    GMM<state, covariance> gmm;
    for (int i = 0; i < 10; ++i) {
        double w = 1.0;
        gmm.add_gaussian(state(0, 0, 0), covariance::Identity(), w);
    }

    GMM<state, covariance>::gauss g = gmm.merge_gaussians(
            gmm.get_gaussians().begin(), gmm.get_gaussians().end());
    EXPECT_DOUBLE_EQ(10.0, g.get_w());
    EXPECT_EQ(state(0, 0, 0), g.get_mu());
    EXPECT_EQ(covariance::Identity(), g.get_cov());
}


TEST (GMM, Template2D) {
    typedef Eigen::Vector2d state;
    typedef Eigen::Matrix2d covariance;
    GMM<state, covariance> gmm;
    for (int i = 0; i < 10; ++i) {
        double w = 1.0;
        gmm.add_gaussian(state(0, 0), covariance::Zero(), w);
    }

    GMM<state, covariance>::gauss g = gmm.merge_gaussians(
            gmm.get_gaussians().begin(), gmm.get_gaussians().end());
    EXPECT_DOUBLE_EQ(10.0, g.get_w());
    EXPECT_EQ(state(0, 0), g.get_mu());
    EXPECT_EQ(covariance::Zero(), g.get_cov());
}

int main(int argc, char **argv) {
    try {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (std::exception &e) {
        std::cerr << "Unhandled Exception: " << e.what() << std::endl;
    }
    return 1;
}
