#include <gtest/gtest.h>
#include <rfs_slam/FilterPHD.h>
#include <boost/make_shared.hpp>
#include <math.h>
#include <boost/lambda/lambda.hpp>

typedef Eigen::Vector3d observation;
typedef Eigen::Matrix3d covariance;
typedef Eigen::Affine2d robot_state;
typedef FilterBase<observation, covariance, robot_state> FB;
typedef FilterPHD<observation, covariance, robot_state> FPHD;

TEST(FilterPHD, Predict) {
    FPHD f;
    FB::vec_vec_observation vec_observations;
    FB::vec_vec_robot_state vec_rss;

    //two robots one with 7 observations and second with 3 observations
    FB::vec_robot_state rss(1, robot_state::Identity());
    vec_rss.push_back(rss);
    vec_rss.push_back(rss);

    vec_observations.push_back(FB::vec_observation(7, observation::Identity()));
    vec_observations.push_back(FB::vec_observation(3, observation::Identity()));
    f.predict(vec_observations, vec_rss);

    EXPECT_EQ(f.phd.size(), 10);
    EXPECT_DOUBLE_EQ(f.phd.sum(), 0.1);
}

int main(int argc, char **argv) {
    try {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (std::exception &e) {
        std::cerr << "Unhandled Exception: " << e.what() << std::endl;
    }
    return 1;
}
