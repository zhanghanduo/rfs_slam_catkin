#include <gtest/gtest.h>
#include <rfs_slam/ParticleFilter.h>
#include <boost/make_shared.hpp>
#include <math.h>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/if.hpp>

TEST(PF, Construct) {
    ParticleFilter<double> pf(100, 215.3);
    ASSERT_EQ(100, pf.particles.size());
    ASSERT_EQ(100, pf.weights.size());
    EXPECT_EQ(pf.particles[10], 215.3);
    EXPECT_EQ(pf.weights[25], 0.01);
}

TEST(PF, WeightShadowing) {
    ParticleFilter<double> pf(100, 215.3);
    pf.set_weight_function(boost::lambda::constant(0.5));
    pf.update_weights();
    EXPECT_EQ(pf.weights[25], 0.5);
}

TEST(PF, Normalize) {
    ParticleFilter<double> pf(100, 215.3);
    pf.set_weight_function(boost::lambda::constant(0.5));
    pf.update_weights();
    pf.normalize_weights();
    EXPECT_EQ(pf.weights[25], 0.01);
}

TEST(PF, NormalizeInverse) {
    ParticleFilter<double> pf(10, 215.3);
    pf.set_weight_function(boost::lambda::constant(1.0));
    pf.update_weights();
    pf.weights[5] = 0.0;
    pf.normalize_weights(true);
    EXPECT_DOUBLE_EQ(pf.weights[5], 1.0);
    EXPECT_DOUBLE_EQ(pf.weights[4], 0.0);
}

TEST(PF, Resample) {
    using namespace boost::lambda;
    ParticleFilter<double> pf(5, 0.0);
    pf.particles[0] = 100.0;
    pf.set_weight_function(boost::lambda::if_then_else_return(_1 > 50.0, 1.0, 0.0));
    pf.update_and_resample();
    BOOST_FOREACH(double p, pf.particles) {
        EXPECT_DOUBLE_EQ(p, 100.0);
    }
}

TEST(PF, MaximumWeight) {
    ParticleFilter<double> pf(10, 215.3);
    pf.weights[5] = 1.0;
    pf.particles[5] = 10.0;
    double max = pf.get_maximum_weight_particle();
    EXPECT_EQ(max, 10.0);
}

int main(int argc, char **argv) {
    try {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (std::exception &e) {
        std::cerr << "Unhandled Exception: " << e.what() << std::endl;
    }
    return 1;
}
