ps = dlmread(['/tmp/poses.csv'])';
%ps = dlmread(['poses_noisy_gmm_dist.csv'])';
folder = [getenv('HOME') '/.ros/rfs_data/ex4/'];
odom = dlmread([folder 'poses_odom.csv'])';
ps(end,:) = [];

save_pdf = true;

T1 = T(odom(:,1));
for i = 1:size(odom,2)
    Tc = T(odom(:,i));
    Ts = inv(T1) * Tc;
    odom(1,i) = Ts(1,3);
    odom(2,i) = Ts(2,3);
end
clear avg;
for i = 1:size(ps,2)
    p = ps(:,i);
    p = reshape(p, 4, length(p)/4);
    avg(:,i) = mean(p');
    avg(3,i) = circular_mean(p(3,:)); %angle
    sum(p(4,:))
end
dlmwrite([folder 'poses_avg.csv'], avg(1:3,:)')

%% Visualization
figure(1)
hold off
plot(p(1,:), p(2,:), '*');
%xlim([-7 5]);
%ylim([-12 1]);
grid on
hold on
plot(odom(1,:) - odom(1,1), odom(2,:) - odom(1,2), '-r');
plot(avg(1,:), avg(2,:), '-k')
axis equal

set(gca,'FontSize',24)
set(gcf,'PaperSize',[15 5])
set(gcf,'PaperPosition',[0 0 15 5])
xlabel x; ylabel y;
title 'Robot Trajectory'
legend('SLAM', 'Odometry')

if(save_pdf)
    drawnow
    print('-dpdf',['/tmp/', 'RFSTrajPosition'])
end

figure(2)
hold off
plot(avg(3,:), '-k', 'LineWidth', 2)
hold on
plot(odom(3,:), '-r')
grid on

set(gca,'FontSize',24)
set(gcf,'PaperSize',[15 5])
set(gcf,'PaperPosition',[0 0 15 5])
xlabel 'time'; ylabel 'angle [rad]';
title 'Robot Trajectory'
legend('SLAM', 'Odometry')
drawnow
if(save_pdf)
    drawnow
    print('-dpdf',['/tmp/', 'RFSTrajAngle'])
end