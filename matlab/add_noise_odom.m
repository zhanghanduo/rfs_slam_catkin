clc;clear all;close all;
folder = [getenv('HOME') '/.ros/rfs_data/ex3/'];
in_odom = [folder 'poses_odom.csv'];
out_odom = [folder 'poses_noisy.csv'];


odom = dlmread(in_odom)';

clear inputs;
for i = 2:size(odom,2)
    inputs{i-1} = inv(T(odom(:,i-1))) * T(odom(:,i));
end

rng(1)
std_lin = .000;
std_ang = -.002;
on(:,1) = odom(:,1);
for i = 1:size(inputs, 2)
    noise = T([std_lin, 0.0, std_ang]);
    p = T(on(:,i)) * inputs{i} * noise;
    on(:,i+1) = p(:,3);
    on(3,i+1) = atan2(p(2,1), p(1,1));
end

plot(odom(1,:), odom(2,:), 'b-*') 
hold on
plot(on(1,:), on(2,:), 'r-*') 

dlmwrite(out_odom, on');