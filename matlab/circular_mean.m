function m = circular_mean(v)
%Compute circular means of values 'v'
%http://en.wikipedia.org/wiki/Mean_of_circular_quantities

n = length(v);

a = sum(sin(v)) / n;
b = sum(cos(v)) / n;
m = atan2(a,b);