function T = T(pose)

T = eye(3);
T(1,1) = cos(pose(3));
T(1,2) = sin(pose(3));
T(2,1) = -sin(pose(3));
T(2,2) = cos(pose(3));
T(1,3) = pose(1);
T(2,3) = pose(2);
T(1:2,1:2) = T(1:2,1:2)'; 
end

