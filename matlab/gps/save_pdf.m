function save_pdf( folder, file )

font_size = 12;
set(gca,'FontSize',font_size,'fontWeight','normal')
set(findall(gcf,'type','text'),'FontSize',font_size,'fontWeight','normal')
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
drawnow
print('-dpdf',[folder, file])

end

