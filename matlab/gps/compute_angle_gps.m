function gps = compute_angle_gps(gps_in)

p = gps_in(:,:);
n = size(p,2);
e = zeros(4,n);
e(:,:) = p;

angle = 0;
k = 20;
for i = k+1:k:n
    v = e(2:3,i)-e(2:3,i-k);
    if norm(v) > 0.001
        a = atan2(v(2),v(1));
        angle = a;
    end
    e(4,i-k:i) = angle;
end
gps = e;

end

