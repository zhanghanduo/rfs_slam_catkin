
clc;close all;clear all;
folder = '/home/voop/Sources/rfs_multi/';
avgp = dlmread([folder 'r1_average_poses.csv'], ',')';
avgpp = dlmread([folder 'r2_average_poses.csv'], ',')';

r_start = avgp(1,1);
r_end = avgpp(1,end);
r1_start = avgp(1,1);
r1_end = avgp(1,end);
r2_start = avgpp(1,1);
r2_end = avgpp(1,end);
folder = '~/.ros/rfs_collect_robot1/';
gps = dlmread([folder 'gps_not_angle.csv'],',')';
u = dlmread([folder 'inputs.csv'], ',')';

gps = gps(:,find_closest_time(gps(1,:), r_start):find_closest_time(gps(1,:), r_end));
u = u(:,find_closest_time(u(1,:), r_start):find_closest_time(u(1,:), r_end));

odom = compute_odometry(u);
gps = compute_angle_gps(gps);
gps = transform_to_origin(gps);

hold off
plot(avgp(2,:), avgp(3,:),'b', 'LineWidth',2)
hold on
tmp = gps(:,find_closest_time(gps(1,:), r1_start):find_closest_time(gps(1,:), r1_end));
plot(tmp(2,:), tmp(3,:), 'r', 'LineWidth',2)
tmp = odom(:,find_closest_time(u(1,:), r1_start):find_closest_time(u(1,:), r1_end));
%plot(tmp(2,:), tmp(3,:), 'k--')

plot(avgpp(2,:), avgpp(3,:),'g', 'LineWidth',2)

tmp = gps(:,find_closest_time(gps(1,:), r2_start):find_closest_time(gps(1,:), r2_end));
plot(tmp(2,:), tmp(3,:), 'r--', 'LineWidth',2)
odom = compute_odometry(u(:,find_closest_time(u(1,:), r2_start):find_closest_time(u(1,:), r2_end)));
for i = 1:size(odom,2)
    odom(2:4,i) = T2D_inv(T2D([ 8.8192;-6.2295;-0.3298]) * T2D(odom(2:4,i)));
end
tmp = odom(:,find_closest_time(odom(1,:), r2_start):find_closest_time(odom(1,:), r2_end));
%plot(tmp(2,:), tmp(3,:), 'k--')
grid on
xlabel 'x [m]'
ylabel 'y [m]'
legend ('Robot 1 Estimation','Robot 1 GPS', 'Robot 2 Estimation','Robot 2 GPS', ...
    'Location', 'NorthOutside')
save_pdf('','real_mvslam_gps')

%%
T1 = T2D(gps(2:4, find_closest_time(gps(1,:), avgp(1,1))));
T2 = T2D(gps(2:4, find_closest_time(gps(1,:), avgpp(1,1))));
B1  = T2D([.225;0;0]);
B2 = B1;
T = T2D_inv(inv(B1) * inv(T1) * T2 * B2 )
