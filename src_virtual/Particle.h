#include <rfs_slam/FilterPHD.h>
#include <rfs_slam/ParticleFilter.h>
#include <rfs_slam/MotionModel.h>

using namespace Eigen;
typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

class Particle {
public:
    Particle() {
        f.JMax = 10000;
        f.Dmin = 0.5;
        f.Tmin = .001;
        f.CK = 0.05;
        f.PD = 0.95;
        f.R_default = Matrix2d::Identity() * 0.05;
        f.fov_H_angle = 0.5 * 270.0 / 180 * M_PI;
        f.fov_V_angle =  0.5 * 270.0 / 180 * M_PI;
        f.fov_distance = 4.0;
        f.fov_distance_min = 0.0;
        mm.a[0] = 0.005; // rad/rad
        mm.a[1] = 2 * (M_PI / 180.0); // rad/meter
        mm.a[2] = 0.02;// meters/meter
        mm.a[3] = 0.01; // meter/rad
        std_lin = std_ang = 0.0;
    }

    Particle(const Particle& p) {
        f = FPHD(p.f);
        rss = p.rss;
        mm = p.mm;
        std_lin = p.std_lin;
        std_ang = p.std_ang;
    }

    virtual ~Particle() {}

    void predict_update_prune(const FPHD::vec_vec_observation& vec_obsvs) {
        if(rss.size() < vec_obsvs.size()) {
            ROS_WARN_STREAM("All observations cannot be incorporated because particle have not enough poses");
            return;
        }
        for(unsigned int i = 0; i < vec_obsvs.size(); ++i) {
            f.predict(vec_obsvs[i], rss[i]);
            f.update(vec_obsvs[i], rss[i]);
            f.prune();
        }
    }

    void motion(const FPHD::vec_robot_state& us) {
        if(rss.size() < us.size()) {
            ROS_WARN_STREAM("All acions cannot be incorporated because particle have not enough poses");
            return;
        }
        for(unsigned int i = 0; i < us.size(); ++i) {
            rss[i] = mm.motion_model_2d_simple(rss[i], us[i], std_lin, std_ang);
            //rss[i] = mm.motion_model_2d(rss[i], us[i]);
        }
    }

    static double compute_weight_gmm_dist(Particle& p, const FPHD::vec_vec_observation& vec_obsvs) {
        if(p.rss.size() < vec_obsvs.size()) {
            ROS_WARN_STREAM("Cannot compute gmm distance from observations (check size)");
            return 0.0;
        }
        FPHD::gmm gmm(1024);//construct GMM from vec_obsvs
        for(unsigned int i = 0; i < vec_obsvs.size(); ++i) {
            BOOST_FOREACH(const Vector2d & z, vec_obsvs[i]) {
                gmm.add_gaussian(p.f.h_inverse(z, p.rss[i]), p.f.R(), 1.0);
            }
        }

        FPHD::vec_robot_state active_states(p.rss.begin(), p.rss.begin() + vec_obsvs.size());
        FPHD::gmm gmm_fov(p.f.phd.size());
        BOOST_FOREACH(const FPHD::gauss & g, p.f.phd.get_gaussians()) {
            if(p.f.is_in_fov(g.get_mu(), active_states) && g.get_w() > 0.5) {
                gmm_fov.add_gaussian(g);
            }
        }
        double dist = gmm.distance_to_gmm(gmm_fov, 0.2, 0);
        return dist;
    }


    static double compute_weight_phd_sum(Particle& p, const FPHD::vec_vec_observation& vec_obsvs) {
        double w = 0.0;
        for(unsigned int i = 0; i < vec_obsvs.size(); ++i) {
            BOOST_FOREACH(const FPHD::vec_observation::value_type & z, vec_obsvs[i]) {
                w += p.f.phd.pdf(p.f.h_inverse(z, p.rss[i]));
            }
        }
        return w;
    }

    static double compute_weight_empty_strategy(Particle& p, const FPHD::vec_vec_observation& vec_obsvs,
            const double m_start, const double m_end) {
        double w = 1.0;
        BOOST_FOREACH(const FPHD::vec_observation & obsvs, vec_obsvs) {
            w *= pow(p.f.CK, obsvs.size());
        }
        w *=  exp(m_end - m_start - p.f.CK - p.f.CK);

        return w;
    }

    static double compute_weight_single_strategy(Particle& p, const FPHD::vec_vec_observation& vec_obsvs,
            const double m_start, const double m_end) {
        double w = 1.0;
        for(int i = 0; i < p.rss.size(); i++) {
            Affine2d rs = p.rss[i];
            FPHD::gauss gauss;
            gauss.set_w(0.0);
            BOOST_FOREACH(const FPHD::gauss & g, p.f.phd.get_gaussians()) {
                if(g.get_w() > gauss.get_w())
                    if(p.f.is_in_fov(g.get_mu(), rs)) {
                        gauss = g;
                    }
            }
            if(gauss.get_w() < 0.1) {
		continue;
            }

            double c = p.f.CK;
            double pd = p.f.pd(gauss.get_mu(), rs);
            double a = (1 - pd) * pow(c, vec_obsvs[i].size());
            double a_sum = 0.0;
            typedef FPHD::vec_observation::value_type observation;
            BOOST_FOREACH(const observation & z, vec_obsvs[i]) {
                a_sum += gauss.pdf(p.f.h_inverse(z, rs)) * pow(c, vec_obsvs[i].size() - 1);
            }
            a += a_sum * pd;
            w *= a;
        }
        w *=  exp(m_end - m_start - p.f.CK - p.f.CK);
        return w;
    }

public:
    FPHD f;
    FPHD::vec_robot_state rss;
    MotionModel mm;
    double std_lin, std_ang;
};
