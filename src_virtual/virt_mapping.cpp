/**
 * Copyright (c) Alpaca  - All Rights Reserved
 * Created on: 02/16/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: SilliconHill
 *    Details: Test close loop - in theory the deviation of the particles should lowerize after loop is closed
 */

#include <ros/ros.h>
#include <rfs_slam/RosUtils.h>
#include <rfs_slam/FilterPHD.h>
#include <rfs_slam/EnvironmentSim.h>
#include <boost/filesystem.hpp>

using namespace Eigen;
typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

void do_mapping(double std = 0.0, double lambda = 0.0, bool save_partial = true) {
    std::stringstream ss;
    ss << "/tmp/virt_mapping/map_" << std << "_" << lambda << "_";
    std::string folder = ss.str();
    FPHD f;
    f.JMax = 10000;
    f.Dmin = 0.5;
    f.Tmin = .001;
    f.CK = lambda * 2;
    f.PD = 0.95;
    f.R_default = Matrix2d::Identity() * (0.02 + std);
    f.fov_H_angle = 0.5 * 270.0 / 180 * M_PI;
    f.fov_V_angle = 0.5 * 270.0 / 180 * M_PI;
    f.fov_distance = 4.0;
    f.fov_distance_min = 0.0;

    boost::shared_ptr<EnvironmentSim> env;
    env.reset(new EnvironmentSim(50, Vector3d(-10, -10, 0), Vector3d(10, 10, 0)));
    env->fov_distance = 4.0;
    env->fov_distance_min = 0.0;
    env->fov_H_angle = 0.5 * 270.0 / 180 * M_PI;
    env->fov_V_angle = 0.5 * 270.0 / 180 * M_PI;
    env->noise_measurement = std;
    if(save_partial)
        env->save_features(folder + "features.csv");
    env->lambda_c = lambda;

    FPHD::vec_robot_state trajectory, trajectory_2;
    env->generate_circle_trajectory(4.0, Vector2d(5, 0), 100, trajectory);
    env->generate_circle_trajectory(3.0, Vector2d(-5, 0), 100, trajectory_2);

    bool first = true;
    ros::Time start = ros::Time::now();
    for(int ll = 0; ll < 2; ll++) {
        for(unsigned int i = 0; i < trajectory.size(); ++i) {
            if(f.phd.size() > 5000) {
                ROS_INFO_STREAM("Number of gaussians over 5000: " << f.phd.size());
            }
            Affine2d rs = trajectory[i];
            Affine2d rs_2 = trajectory_2[i];
            FPHD::vec_observation obsvs;
            env->get_observation(obsvs, rs);
            f.predict(obsvs, rs);
            f.update(obsvs, rs);
            f.prune();

            if(save_partial) {
                ROSUtils::save_observations(obsvs, folder + "obsvs1.csv", first);
                ROSUtils::save_pose(rs, folder + "pose1.csv", first);
            }
            env->get_observation(obsvs, rs_2);
            f.predict(obsvs, rs_2);
            f.update(obsvs, rs_2);
            f.prune();

            if(save_partial) {
                ROSUtils::save_observations(obsvs, folder + "obsvs2.csv", first);
                ROSUtils::save_pose(rs_2, folder + "pose2.csv", first);
                ROSUtils::save_weights(f, folder + "weights.csv", first);
                ROSUtils::save_mu(f, folder + "mu.csv", first);
                ROSUtils::save_cov(f, folder + "cov.csv", first);
            }
            //save ground truth observations
            env->noise_measurement = 0.0;
            env->lambda_c = 0.0;
            env->get_observation(obsvs, rs);
            if(save_partial)
                ROSUtils::save_observations(obsvs, folder + "obsvs1_gt.csv", first);
            env->get_observation(obsvs, rs_2);
            if(save_partial)
                ROSUtils::save_observations(obsvs, folder + "obsvs2_gt.csv", first);
            env->noise_measurement = std;
            env->lambda_c = lambda;

            first = false;
//             ROSUtils::publish_phd(f);
//             ROSUtils::publish_transform(rs, "r_1");
//             ROSUtils::publish_transform(rs_2, "r_2");
        }
    }

    ros::Time end = ros::Time::now();
    double elapsed_time = end.toSec() - start.toSec();
    std::ofstream file;
    file.open((folder + "peformance.csv").c_str());
    file << elapsed_time << std::endl;
    file.close();
    ROS_INFO_STREAM(std << "-" << lambda << " : " << elapsed_time);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "mapping");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool save_partial = true;
    node.param("save_partial", save_partial, true);
    ROS_INFO_STREAM("Save partial results: " << save_partial);

    boost::filesystem::path path("/tmp/virt_mapping");
    boost::filesystem::create_directories(path);

    std::vector<double> stds;
    std::vector<double> lambdas;

    stds.push_back(0.0);
    stds.push_back(0.01);
    stds.push_back(0.05);
    stds.push_back(0.1);
    stds.push_back(0.2);
    stds.push_back(0.5);
    lambdas.push_back(0.0);
    lambdas.push_back(0.1);
    lambdas.push_back(0.3);
    lambdas.push_back(0.5);


    std::vector<std::pair <double, double> > scenarios;
    BOOST_FOREACH(double std, stds) {
        BOOST_FOREACH(double lambda, lambdas) {
            scenarios.push_back(std::make_pair(std, lambda));
        }
    }

    #pragma omp parallel for
    for(unsigned int i = 0; i < scenarios.size(); ++i) {
        std::pair <double, double> sc = scenarios[i];
        do_mapping(sc.first, sc.second, save_partial);
    }

    return 0;
}
