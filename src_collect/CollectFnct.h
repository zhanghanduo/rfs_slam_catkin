#ifndef COLLECTFNCT_H
#define COLLECTFNCT_H

#include <pcl/io/pcd_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/voxel_grid.h>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/passthrough.h>

namespace fs = ::boost::filesystem;
typedef typename std::vector<Eigen::Affine2d, Eigen::aligned_allocator<Eigen::Affine2d> > affine_vec;
using std::string;
using std::vector;


class CollectFnct {
public:
    CollectFnct() {}

    virtual ~CollectFnct() {}

    /** \brief Get all files from the directory */
    void get_all_files(const fs::path& root, std::vector<fs::path>& ret);

    /** \brief Get file name based on the ros time */
    std::string get_file_name(ros::Time t);
    /** \brief Extract ros time from the formated string */
    ros::Time extract_time(const std::string& str);
    /** \brief Print ROS time user-friendly */
    void print_time(ros::Time t);

    /** \brief Write LaserScan to file */
    void write_scan(const sensor_msgs::LaserScanConstPtr& msg, std::string path);
    /** \brief Read LaserScan from file */
    bool read_scan(sensor_msgs::LaserScan& msg, const std::string& path);

    /** \brief Transform the point cloud 'pc' using the transform t */
    void transform_pc(pcl::PCLPointCloud2& pc, const tf::Transform& t) {
        sensor_msgs::PointCloud2 rpc, rpcout;
        pcl_conversions::fromPCL(pc, rpc);
        pcl_ros::transformPointCloud("odom", t, rpc, rpcout);
        pcl_conversions::toPCL(rpcout, pc);
    }

    /** \brief Load GPS data from the file - relative positions w.r.t. base station */
    bool load_gps(const string& filename, vector<ros::Time>& times, affine_vec& poses);

private:
    /** \brief Apply the pass throught filter to the xtion data */
    void pass_throught_xtion(pcl::PCLPointCloud2::Ptr& pc) {
        pcl::PCLPointCloud2::Ptr fptcloud(new pcl::PCLPointCloud2());
        pcl::PassThrough<pcl::PCLPointCloud2> pass;
        pass.setInputCloud(pc);
        pass.setFilterFieldName("z");
        pass.setFilterLimits(0.8, 3.5);
        pass.filter(*fptcloud);
        pc = fptcloud;
    }
};

bool CollectFnct::load_gps(const string& filename, vector< ros::Time >& times, affine_vec& poses)  {
    using namespace boost::posix_time;
    using namespace Eigen;

    std::ifstream f(filename.c_str());
    if(!f.is_open())
        return false;

    std::string line;
    while(std::getline(f, line)) {
        if(line.substr(0, 10) == "$PTNL,VGK,") {
            std::stringstream ss;
            time_input_facet* facet = new time_input_facet("D%y-%m-%d$PTNL,VGK,%H%M%S%F,");
            ss.imbue(std::locale(ss.getloc(), facet));
            ss.str("D14-04-28" + line);
            ptime pt;
            double x, y, z;
            char c;
            int gps_quality, date;
            ss.str(line.substr(20, line.length()));
            ss >> date >> c >> x >> c >> y >> c >> z >> c >> gps_quality >> c;

            if(gps_quality != 3) {
                ROS_WARN_STREAM("GPS not fixed, quality: " << gps_quality);
            }
            Affine2d e = Affine2d::Identity();
            double angle = 0;

            if(!poses.empty()) {
                Vector2d v(x, y), v_last = poses.back().translation();
                Vector2d vd = v - v_last;
                angle = atan2(vd(0), vd(1));
            }
            e = Translation2d(x, y) * Rotation2Dd(angle);
            poses.push_back(e);
            times.push_back(ros::Time::fromBoost(pt));
        }
    }

    times.erase(times.begin());
    poses.erase(poses.begin());
    f.close();
}

std::string CollectFnct::get_file_name(ros::Time t)  {
    using namespace boost::posix_time;
    std::stringstream ss;
    time_facet* facet = new time_facet("%y-%m-%dT%H-%M-%S%F");
    ss.imbue(std::locale(ss.getloc(), facet));
    ss << "D" << ros::Time::now().toBoost();
    return ss.str();
}

ros::Time CollectFnct::extract_time(const std::string& str) {
    using namespace boost::posix_time;
    std::stringstream ss;
    time_input_facet* facet = new time_input_facet("D%y-%m-%dT%H-%M-%S%F");
    ss.imbue(std::locale(ss.getloc(), facet));
    ss.str(str);
    ptime pt;
    ss >> pt;
    ros::Time t = ros::Time::fromBoost(pt);
    return t;
}

void CollectFnct::print_time(ros::Time t) {
    using namespace boost::posix_time;
    std::stringstream ss;
    time_facet* facet = new time_facet("%y-%m-%d   %H:%M:%S%F");
    ss.imbue(std::locale(ss.getloc(), facet));
    ss << t.toBoost();
    ROS_INFO_STREAM(ss.str());
}

void CollectFnct::write_scan(const sensor_msgs::LaserScanConstPtr& msg, std::string path)  {
    std::ofstream ofs(path.c_str(), std::ios::out | std::ios::binary);
    uint32_t serial_size = ros::serialization::serializationLength(*msg);
    boost::shared_array<uint8_t> obuffer(new uint8_t[serial_size]);
    ros::serialization::OStream ostream(obuffer.get(), serial_size);
    ros::serialization::serialize(ostream, *msg);
    ofs.write((char*) obuffer.get(), serial_size);
    ofs.close();
}

bool CollectFnct::read_scan(sensor_msgs::LaserScan& msg, const std::string& path)  {
    std::ifstream ifs(path.c_str(), std::ios::in | std::ios::binary);
    if(!ifs.is_open())
        return false;
    ifs.seekg(0, std::ios::end);
    std::streampos end = ifs.tellg();
    ifs.seekg(0, std::ios::beg);
    std::streampos begin = ifs.tellg();

    uint32_t file_size = end - begin;
    boost::shared_array<uint8_t> ibuffer(new uint8_t[file_size]);
    ifs.read((char*) ibuffer.get(), file_size);
    ros::serialization::IStream istream(ibuffer.get(), file_size);
    ros::serialization::deserialize(istream, msg);
    ifs.close();
    return true;
}

void CollectFnct::get_all_files(const boost::filesystem::path& root, std::vector< boost::filesystem::path >& ret) {
    if(!fs::exists(root)) return;

    if(fs::is_directory(root)) {
        fs::recursive_directory_iterator it(root);
        fs::recursive_directory_iterator endit;
        while(it != endit) {
            if(fs::is_regular_file(*it)) {
                ret.push_back(it->path());
            }
            ++it;
        }
    }
    std::sort(ret.begin(), ret.end());
}

#endif // COLLECTFNCT_H
