/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 04/28/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Load GPS logged data
 */

#include <ros/ros.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <Eigen/Geometry>
#include <Eigen/StdVector>
//#include "CollectFnct.h"
#include <rfs_slam/RosUtils.h>

typedef typename std::vector<Eigen::Affine2d, Eigen::aligned_allocator<Eigen::Affine2d> > affine_vec;
using std::string;
using std::vector;
using namespace Eigen;
bool get_pose(const ros::Time& timestamp, const vector<ros::Time>& times, const affine_vec& poses,
              Affine2d& pose) {
    int int_t_low = -1;

    for(unsigned int i = 0; i < times.size() - 1; ++i) {
        if(times[i] > timestamp && times[i + 1] < timestamp) {
            int_t_low = i;
            break;
        }
    }
    if(int_t_low == -1)
        return false;

    //TODO: linear interpolation of the pose
    ros::Duration dt_low = times[int_t_low] - timestamp;
    ros::Duration dt_up = times[int_t_low + 1] - timestamp;
    if(fabs(dt_low.toSec()) < fabs(dt_up.toSec())) {
        pose = poses[int_t_low];
    } else {
        pose = poses[int_t_low + 1];
    }
    return true;
}

bool load_gps(const string& filename, vector< ros::Time >& times, affine_vec& poses)  {
    using namespace boost::posix_time;
    using namespace Eigen;

    std::ifstream f(filename.c_str());
    if(!f.is_open())
        return false;

    std::string line;
    while(std::getline(f, line)) {
        if(line.substr(0, 10) == "$PTNL,VGK,") {
            std::stringstream ss;
            time_input_facet* facet = new time_input_facet("D%y-%m-%d$PTNL,VGK,%H%M%S%F,");
            ss.imbue(std::locale(ss.getloc(), facet));
            ss.str("D14-04-28" + line);
            ptime pt;
            ss >> pt;
            double x, y, z;
            char c;
            int gps_quality, date;
            ss.str(line.substr(20, line.length()));
            ss >> date >> c >> x >> c >> y >> c >> z >> c >> gps_quality >> c;

            if(gps_quality != 3) {
                ROS_WARN_STREAM("GPS not fixed, quality: " << gps_quality);
            }
            Affine2d e = Affine2d::Identity();
            Rotation2Dd rot = Rotation2Dd::Identity();

            if(poses.size() > 2) {
                Vector2d A, B, C(x, y);
                A = poses[poses.size() - 2].translation();
                B = poses[poses.size() - 1].translation();
                Vector2d v1 = B - A, v2 = C - B;
                double eps = 0.001;
//  angle = 0;
// k = 10;
// for i = k+1:k:n
//     v = e(1:2,i)-e(1:2,i-k);
//     if norm(v) > 0.001
//         angle = atan2(v(2),v(1));
//     end
//     e(3,i-k:i) = angle;
// end
                //TODO: compute angle, for now matlab version
                rot = Rotation2Dd(0.0);
            }
            e = Translation2d(x, y) * rot;
            poses.push_back(e);
            times.push_back(ros::Time::fromBoost(pt));
        }
    }
    poses.pop_back();
    times.pop_back();
    f.close();
}


bool load_u(const string& filename, vector< ros::Time >& times, vector<double>& left, vector<double>& right)  {
    using namespace boost::posix_time;
    using namespace Eigen;

    std::ifstream f(filename.c_str());
    if(!f.is_open())
        return false;

    std::string line;
    while(std::getline(f, line)) {
        std::stringstream ss;
        //21:28:55.081 0.0000 0.0000
        time_input_facet* facet = new time_input_facet("D%y-%m-%dT%H:%M:%S%F");
        ss.imbue(std::locale(ss.getloc(), facet));
        ss.str("D14-04-28T" + line);
        ptime pt;
        ss >> pt;
        double u_left, u_right;
        char c;
        ss.str(line.substr(12, line.length()));
        ss >> u_left >> c >> u_right;

        left.push_back(u_left);
        right.push_back(u_right);
        times.push_back(ros::Time::fromBoost(pt) - ros::Duration(2 * 60 * 60));
    }
    f.close();
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "load_gps");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    

    std::string folder;
    node.param("/folder", folder, std::string("") + getenv("HOME") + "/.ros/rfs_collect/");
    ROS_INFO_STREAM(folder);

    vector<ros::Time> times, times2;
    affine_vec poses;
    vector<double> left, right;
    load_gps(folder + "gps.log", times, poses);
    load_u(folder +  "u.log", times2, left, right);

    {
        std::ofstream f((folder + "gps_not_angle.csv").c_str());
        f.precision(20);
        for(int i = 0; i < poses.size(); i++) {
            Affine2d e = poses[i];
            f << times[i].toSec() << "," << e.translation()[0] << "," << e.translation()[1] << ",";
            Eigen::Rotation2Dd rot(0);
            rot.fromRotationMatrix(e.rotation());
            f << rot.angle();
            f << std::endl;
        }
        f.close();
    }

    {
        std::ofstream f((folder + "inputs.csv").c_str());
        f.precision(20);
        for(int i = 0; i < times2.size(); i++) {
            f << times2[i].toSec() << ",";
            f << left[i] << ",";
            f << right[i];
            f << std::endl;
        }
        f.close();
    }


    return 0;
}
