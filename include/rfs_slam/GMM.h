/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 16, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 *    Details: Gaussian mixture model for representing probability distribution.
 */

#ifndef GMM_H_
#define GMM_H_

#include <rfs_slam/Gauss.h>
#include <boost/foreach.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

template<class state, class covariance>
class GMM {
public:
    typedef GMM<state, covariance> gmm;
    typedef Gauss<state, covariance> gauss;
    typedef std::vector<gauss> vec_gauss;
    typedef typename vec_gauss::iterator vec_gauss_it;
    typedef typename vec_gauss::const_iterator vec_gauss_cit;

public:
    GMM();
    /**\brief Create GMM with allocation of n gaussians */
    GMM(unsigned int n);
    virtual ~GMM();

    /** \brief Compute pdf of the PHD at the state defined by the Vector v */
    double pdf(const state& x) const;

    /** \brief Compute PDF of the i-th gaussian */
    double pdf(unsigned int i, const state& x) const;

    /** \brief Add gaussian to the GMM.
     *  \return index of the gaussian */
    int add_gaussian(const state& mu, const covariance& cov,
                     const double& weight);
    /** \brief Add gaussian to the GMM.
     *  \return index of the gaussian */
    int add_gaussian(const gauss& g);

    /** \brief Remove i-th gaussian from the GMM. */
    bool remove_gaussian(unsigned int i);

    /** \brief Prune the PHD.
     *  \param Dmin - Cluster components with mahalanobis distance squared lower then Dmin.
     *  \param Tmin - Remove components with weight lowest then Tmin.
     *  \param Jmax - Maximum number of gaussians.
     */
    void prune(double Dmin, double Tmin, unsigned int Jmax);

    /** \brief Number of the gaussian in the PHD. */
    unsigned int size() const;

    /** \brief Sum of all gaussian sum(w,0,size());*/
    double sum() const;

    /** \brief Get pointer to gaussian with the largest weight*/
    vec_gauss_cit get_largest_weight_gauss() const;

    /** \brief Merge gaussians between the two iterators. */
    gauss merge_gaussians(vec_gauss_cit begin, vec_gauss_cit end) const;

    /** \brief Get vector of gaussians */
    const vec_gauss& get_gaussians() const;
    /** \brief Get non-cost vector of gaussians */
    vec_gauss& get_gaussians();

    /** \brief Compute distance between this and another gmm representation
        \param c is cut-off parameter
        \param d is metric wich will be used {0 = euclidean, 1 = mahalanobis}
        \details dist = sum( min{c^2, d(mu1, mu2)^2} ) / size() */
    double distance_to_gmm(const gmm& g, double c = 0.05, unsigned int d = 0);

private:
    vec_gauss gs;
};

template<class state, class covariance>
inline double GMM<state, covariance>::distance_to_gmm(const gmm& l, double c, unsigned int d) {
    if(size() == 0)
        return 0.0;
    double dist = 0;
    BOOST_FOREACH(const gauss& g, this->get_gaussians()) {
        double min_dist = pow(c, 2);
        BOOST_FOREACH(const gauss& gl, l.get_gaussians()) {
            double gauss_dist;
            if(d == 0) { //euclidean distance
                gauss_dist = (g.get_mu() - gl.get_mu()).squaredNorm();
            } else { //mahalanobis distance
                gauss_dist = g.mahalanobis_distance_squared(gl.get_mu());
            }
            if(gauss_dist < min_dist) {
                min_dist = gauss_dist;
            }
        }
        dist += min_dist;
    }
    dist = dist / ((double) this->size());
    return dist;
}

template<class state, class covariance>
inline GMM<state, covariance>::GMM() {
}

template<class state, class covariance>
inline GMM<state, covariance>::GMM(unsigned int n) {
    gs.reserve(n);
}

template<class state, class covariance>
inline GMM<state, covariance>::~GMM() {
}

template<class state, class covariance>
inline double GMM<state, covariance>::pdf(const state& x) const {
    double pdf = 0.0;
    BOOST_FOREACH(const gauss& g, gs) {
        pdf += g.pdf(x);
    }
    return pdf;
}

template<class state, class covariance>
inline double GMM<state, covariance>::pdf(unsigned int i, const state& x) const {
    return gs[i].pdf(x);
}

template<class state, class covariance>
inline int GMM<state, covariance>::add_gaussian(const state& mu, const covariance& cov,
        const double& weight) {
    gs.push_back(gauss(mu, cov, weight));
    return size() - 1;
}

template<class state, class covariance>
inline int GMM<state, covariance>::add_gaussian(const gauss& g) {
    gs.push_back(g);
    return size() - 1;
}

template<class state, class covariance>
inline bool GMM<state, covariance>::remove_gaussian(unsigned int i) {
    gs.erase(gs.begin() + i);
    return true;
}

template<class state, class covariance>
inline void GMM<state, covariance>::prune(double Dmin, double Tmin,
        unsigned int Jmax) {
    using namespace boost::lambda;
    if (Tmin > 0) { //Remove lower than Tmin
        gs.erase(
            std::remove_if(gs.begin(), gs.end(),
                           bind(&gauss::get_w, boost::lambda::_1) < Tmin),
            gs.end());
    }

    if (Dmin >= 0) { //Merge gaussian with mahalanobis distance squared lower then Dmin
        vec_gauss gs_new;
        gs_new.reserve(gs.capacity()); //preserve capacity
        while (!gs.empty()) {

            state x = get_largest_weight_gauss()->get_mu();
            vec_gauss gs_merge(gs.size());
            vec_gauss_it sep_cp = std::remove_copy_if(gs.begin(), gs.end(),
                                  gs_merge.begin(),
                                  !(bind(&gauss::mahalanobis_distance_squared,
                                         boost::lambda::_1, boost::lambda::constant(x))
                                    <= Dmin));
            gs_new.push_back(merge_gaussians(gs_merge.begin(), sep_cp));

            vec_gauss_it separator = std::remove_if(gs.begin(), gs.end(),
                                                    (bind(&gauss::mahalanobis_distance_squared,
                                                            boost::lambda::_1, boost::lambda::constant(x))
                                                            <= Dmin));
            gs.erase(separator, gs.end());
        }
        gs = gs_new;
    }

    if (size() > Jmax) { //Cut to maximum of Jmax elements
        int n = Jmax;
        std::nth_element(gs.begin(), gs.begin() + n, gs.end(),
                         bind(&gauss::get_w, boost::lambda::_1)
                         > bind(&gauss::get_w, boost::lambda::_2));
        gs.erase(gs.begin() + n, gs.end());
    }
}

template<class state, class covariance>
inline unsigned int GMM<state, covariance>::size() const {
    return gs.size();
}

template<class state, class covariance>
inline typename GMM<state, covariance>::vec_gauss_cit GMM<state, covariance>::get_largest_weight_gauss() const {
    using boost::lambda::_1;
    using boost::lambda::_2;
    using boost::lambda::bind;
    return std::max_element(gs.begin(), gs.end(),
                            bind(&gauss::get_w, _1) < bind(&gauss::get_w, _2));
}

template<class state, class covariance>
inline typename GMM<state, covariance>::gauss GMM<state, covariance>::merge_gaussians(
    vec_gauss_cit begin, vec_gauss_cit end) const {
    state mu = state::Zero();
    double w(0);
    covariance cov = covariance::Zero();

    BOOST_FOREACH(const gauss& g, std::make_pair(begin,end)) {
        w += g.get_w();
        mu += g.get_w() * g.get_mu();
    }
    mu /= w;

    BOOST_FOREACH(const gauss& g, std::make_pair(begin,end)) {
        state v = mu - g.get_mu();
        cov += g.get_w() * (g.get_cov() + v * v.transpose());
    }
    cov /= w;

    return gauss(mu, cov, w);
}

template<class state, class covariance>
inline const typename GMM<state, covariance>::vec_gauss& GMM<state, covariance>::get_gaussians() const {
    return gs;
}

template<class state, class covariance>
inline typename GMM<state, covariance>::vec_gauss& GMM<state, covariance>::get_gaussians() {
    return gs;
}

template<class state, class covariance>
inline double GMM<state, covariance>::sum() const {
    double sum = 0.0;
    BOOST_FOREACH(const gauss& g, gs) {
        sum += g.get_w();
    }
    return sum;
}

#endif /* GMM_H_ */
