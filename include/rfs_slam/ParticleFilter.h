/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Dec 7, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 *    Details: Particle filter template implementation.
 */

#ifndef PARTICLEFILTER_H_
#define PARTICLEFILTER_H_

#include <boost/function.hpp>
#include <boost/random.hpp>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/range/combine.hpp>

/** \brief Patricle filter class expect each particle is clonable i.e. particle(p) will clone the particle */
template<class particle>
class ParticleFilter {
public:
    typedef boost::function<double(particle&)> weight_function_ptr;
private:
    typedef boost::tuple<double&, particle&> pw_tuple;

public:
    ParticleFilter(unsigned int number_of_particles,
            const particle& initial_state);
    virtual ~ParticleFilter();

    /** \brief Set function which will be used for particle weighting */
    void set_weight_function(weight_function_ptr f);

    /* \brief Compute new particles weight and resample them based on discrete distribution
     * with probabilities equals to  normalized weights 
     * \param inverse_weight whether weighting function is inverse i.e. all weights are inverted after normalizing*/
    void update_and_resample(bool inverse_weight = false);
    /* \brief Update weight of the particles by calling function which was set by set_weight_function */
    void update_weights();
    /** \brief Resample particles based on discrete distribution with probabilities equals to normalized weights*/
    void resample();
    /** \brief Normalize particle weights to sum to 1.0 
     *  \param inverse_weight whether weighting function is inverse i.e. all weights are inverted after normalizing */
    void normalize_weights(bool inverse_weight = false);
    /** \brief Get particle with maximum weight */
    particle get_maximum_weight_particle();

public:
    std::vector<particle> particles;
    std::vector<double> weights;

private:
    weight_function_ptr fnct_w;

};

template<class particle>
inline ParticleFilter<particle>::ParticleFilter(
        unsigned int number_of_particles, const particle& initial_state) {
    double w = 1.0 / number_of_particles;
    weights.resize(number_of_particles, w);
    particles.resize(number_of_particles, initial_state);
}

template<class particle>
inline ParticleFilter<particle>::~ParticleFilter() {
}

template<class particle>
inline void ParticleFilter<particle>::update_weights() {
    if (!fnct_w) {
        std::cout << "Weighting function is not set - cannot update weights! "
                << std::endl;
        return;
    }
    using namespace boost;
    BOOST_FOREACH(pw_tuple p, combine(weights, particles)) {
        get<0>(p) = fnct_w(get<1>(p));
    }
}

template<class particle>
inline void ParticleFilter<particle>::resample() {
    static boost::mt19937 gen(time(NULL));
    boost::random::discrete_distribution<> dist(weights.begin(), weights.end());

    std::vector<particle> new_ps;
    new_ps.reserve(particles.size());
    for (unsigned int i = 0; i < particles.size(); ++i) {
        new_ps.push_back(particles[dist(gen)]);
    }
    particles = new_ps;
}

template<class particle>
inline void ParticleFilter<particle>::set_weight_function(
        weight_function_ptr f) {
    fnct_w = f;
}

template<class particle>
inline void ParticleFilter<particle>::update_and_resample(bool inverse_weight) {
    update_weights();
    normalize_weights(inverse_weight);
    resample();
}

template<class particle>
inline void ParticleFilter<particle>::normalize_weights(bool inverse_weight) {
   
    if(inverse_weight) {
        double maximum = *std::max_element(weights.begin(),weights.end());
        BOOST_FOREACH(double& w, weights) {
            w = maximum - w;
        }   
    }
    
    double sum = 0.0;
    BOOST_FOREACH(const double& w, weights) {
        sum += w;
    }
    if (std::numeric_limits<double>::epsilon() > sum)
        sum = weights.size();
    BOOST_FOREACH(double& w, weights) {
        w /= sum;
    }        
}

template<class particle>
inline particle ParticleFilter<particle>::get_maximum_weight_particle() {
    unsigned int ind = std::distance(weights.begin(),
            std::max_element(weights.begin(), weights.end()));
    return particles[ind];
}

#endif /* PARTICLEFILTER_H_ */
