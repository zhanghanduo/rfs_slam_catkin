/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 02/16/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Utilities for ros - mostly publisher and saver of the data
 */

#ifndef ROSUTILS_H_
#define ROSUTILS_H_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <sensor_msgs/Range.h>
#include <rfs_slam/FilterPHD.h>
#include <iostream>
#include <ostream>

namespace ROSUtils {
    using namespace Eigen;
    typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

    void save_poses(const FPHD::vec_robot_state& rss, const std::vector<double>& weights, const std::string& filename, bool first = false) {
        std::ofstream of;
        if(first) {
            of.open(filename.c_str(), std::ios::out);
        } else
            of.open(filename.c_str(), std::ios::out | std::ios::app);

        for(unsigned int i = 0; i < rss.size(); ++i) {
            const Eigen::Affine2d rs = rss[i];
            of << rs.translation()(0) << ",";
            of << rs.translation()(1) << ",";
            Eigen::Rotation2Dd rot(0);
            rot.fromRotationMatrix(rs.rotation());
            of << rot.angle() << ",";
            of << weights[i] << ",";
        }
        of << std::endl;
        of.close();

    }

    void save_pose(const Eigen::Affine2d& rs, const std::string& filename, bool first) {
        std::ofstream of;
        if(first) {
            of.open(filename.c_str(), std::ios::out);
        } else
            of.open(filename.c_str(), std::ios::out | std::ios::app);

        of << rs.translation()(0) << ",";
        of << rs.translation()(1) << ",";
        Eigen::Rotation2Dd rot(0);
        rot.fromRotationMatrix(rs.rotation());
        of << rot.angle() << ",";
        of << std::endl;
        of.close();
    }

    void save_weights(const FPHD::FB& f, const std::string& filename, bool first) {
        std::ofstream of;
        if(first) {
            of.open(filename.c_str(), std::ios::out);
        } else
            of.open(filename.c_str(), std::ios::out | std::ios::app);

        for(unsigned int i = 0; i < f.phd.size(); ++i) {
            double w = f.phd.get_gaussians().at(i).get_w();
            of << w << ",";
        }
        of << std::endl;
        of.close();
    }

    void save_mu(const FPHD::FB& f, const std::string& filename, bool first) {
        std::ofstream of;
        if(first) {
            of.open(filename.c_str(), std::ios::out);
        } else
            of.open(filename.c_str(), std::ios::out | std::ios::app);

        for(unsigned int i = 0; i < f.phd.size(); ++i) {
            FPHD::gauss g;
            g = f.phd.get_gaussians().at(i);
            of << g.get_mu()[0] << "," << g.get_mu()[1] << ",";
        }
        of << std::endl;
        of.close();
    }

    void save_cov(const FPHD::FB& f, const std::string& filename, bool first) {
        std::ofstream of;
        if(first) {
            of.open(filename.c_str(), std::ios::out);
        } else
            of.open(filename.c_str(), std::ios::out | std::ios::app);

        for(unsigned int i = 0; i < f.phd.size(); ++i) {
            FPHD::gauss g;
            g = f.phd.get_gaussians().at(i);
            of << g.get_cov()(0, 0) << "," << g.get_cov()(0, 1) << "," << g.get_cov()(1, 0) << "," << g.get_cov()(1, 1) << ",";
        }
        of << std::endl;
        of.close();
    }

    void save_observations(const FPHD::vec_observation& z, const std::string& filename, bool first) {
        std::ofstream of;
        if(first) {
            of.open(filename.c_str(), std::ios::out);
        } else
            of.open(filename.c_str(), std::ios::out | std::ios::app);

        if(z.empty()) {
            of << "0,0,";
        }
        for(unsigned int i = 0; i < z.size(); ++i) {
            of << z[i](0) << "," << z[i](1) << ",";
        }
        of << std::endl;
        of.close();
    }

    void publish_vision(double min_range, double max_range, double angle, const std::string& frame) {
        static ros::NodeHandle node;
        static ros::Publisher pub = node.advertise<sensor_msgs::Range>("/range", 1);
        sensor_msgs::Range range;
        range.field_of_view = angle;
        range.radiation_type = sensor_msgs::Range::ULTRASOUND;
        range.header.frame_id = frame;
        range.min_range = min_range;
        range.max_range = max_range;
        range.range = max_range;

        pub.publish(range);
    }

    void publish_poses(const FPHD::vec_robot_state& rss, const std::vector<double>& weights) {
        static ros::NodeHandle node;
        static ros::Publisher pub = node.advertise<geometry_msgs::PoseArray>("/particles", 1);
        if(pub.getNumSubscribers() == 0)
            return;

        geometry_msgs::PoseArray poses;
        poses.header.frame_id = "/world";

        geometry_msgs::Pose pose;
        BOOST_FOREACH(const Eigen::Affine2d & rs, rss) {
            pose.position.x = rs.translation()(0);
            pose.position.y = rs.translation()(1);
            pose.position.z = 0;
            Eigen::Rotation2Dd rot(0);
            rot.fromRotationMatrix(rs.rotation());
            pose.orientation = tf::createQuaternionMsgFromYaw(rot.angle());
            poses.poses.push_back(pose);
        }

        pub.publish(poses);
    }

    void publish_transform(const Affine2d& rs, const std::string& frame) {
        static tf::TransformBroadcaster br;
        tf::StampedTransform stamped_transform;
        stamped_transform.frame_id_ = "world";
        stamped_transform.child_frame_id_ = frame;
        stamped_transform.stamp_ = ros::Time::now();
        Affine3d e;
        e.setIdentity();
        Vector3d v(0, 0, 0);
        v.head(2) = rs.translation();
        Matrix3d rot = Matrix3d::Identity();
        rot.topLeftCorner(2, 2) = rs.rotation();
        e.translate(v).rotate(rot);
        tf::transformEigenToTF(e, stamped_transform);
        br.sendTransform(stamped_transform);
    }

    void publish_phd(const FPHD::FB& filter) {
        static ros::NodeHandle node;
        static ros::Publisher pub_phd(node.advertise<pcl::PointCloud<pcl::PointXYZI> >("/phd", 1));
        if(pub_phd.getNumSubscribers() == 0)
            return;
        float voxel_size = 0.05;
        pcl::PointCloud<pcl::PointXYZI>::Ptr pc(
            new pcl::PointCloud<pcl::PointXYZI>);

        BOOST_FOREACH(const FPHD::gauss & g, filter.phd.get_gaussians()) {
            std::vector<double> eigs;
            FPHD::vec_observation eigvs;
            g.eigen_value_vectors(eigs, eigvs);
            double length = *std::max_element(eigs.begin(), eigs.end());
            length = sqrt(length) * 2;
            if(length > 1.0) {
                ROS_WARN_STREAM("Eigen value 2*sqrt() too large: " << length);
                length = 1.0;
            }
            if(length < 0.05) {
                ROS_WARN_STREAM("Eigen value 2*sqrt() too small: " << length);
                length = 0.1;
            }
            for(float xx = -length; xx <= length; xx += voxel_size) {
                for(float yy = -length; yy <= length; yy += voxel_size) {
                    Vector2d state = g.get_mu();
                    state(0) += xx;
                    state(1) += yy;
                    pcl::PointXYZI p;
                    p.x = state(0);
                    p.y = state(1);
                    p.z = 0.0;
                    p.intensity = filter.phd.pdf(state);
                    //p.z += (p.intensity);
                    pc->push_back(p);
                }
            }
        }

        std_msgs::Header header;
        header.frame_id = "world";
        header.stamp = ros::Time::now();
        pc->header = pcl_conversions::toPCL(header);

        pcl::VoxelGrid<pcl::PointXYZI> sor;
        sor.setInputCloud(pc);
        sor.setLeafSize(voxel_size, voxel_size, voxel_size);

        pcl::PointCloud<pcl::PointXYZI> outpc;
        sor.filter(outpc);
        pub_phd.publish(outpc);

    }

};
#endif
