/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 28, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 *    Details: Environment simulator for multi-robot slam.
 */

#ifndef ENVIRONMENTSIM_H_
#define ENVIRONMENTSIM_H_

#include <vector>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <boost/random.hpp>

using std::vector;
using namespace Eigen;
class EnvironmentSim {
public:
    /*\brief Simulation with 100 features */
    EnvironmentSim();
    /*\brief Simulation with num_of_features features */
    EnvironmentSim(unsigned int num_of_features);
    /*\brief Simulation with num_of_features features in environmend of defined size */
    EnvironmentSim(unsigned int num_of_features, const Vector3d& map_dim_min,
            const Vector3d& map_dim_max);
    virtual ~EnvironmentSim();

    /**\brief generate random features in the environment */
    void generate_random_features(unsigned int num_of_features = 100);

    /** \brief is feature in fov of robot */
    bool is_in_fov(const Vector3d& feature, const Affine2d& rs) const;

    void get_observation(
            vector<Vector3d, aligned_allocator<Vector3d> >& observation,
            const Affine2d& rs);
    void get_observation(
            vector<Vector2d, aligned_allocator<Vector2d> >& observation,
            const Affine2d& rs);

    Vector2d h2(const Vector3d& feature, const Affine2d& rs) const;
    Vector3d h3(const Vector3d& feature, const Affine2d& rs) const;

    /** \brief Generate circle trajectory around center with radius */
    void generate_circle_trajectory(double radius, const Vector2d& center,
            unsigned int n,
            vector<Affine2d, aligned_allocator<Affine2d> >& traj, double angle_offset = 0);
    
    void save_features(const std::string& filename);

public:
    Vector3d map_dim_min;
    Vector3d map_dim_max;
    double fov_distance, fov_distance_min, fov_H_angle, fov_V_angle;
    double noise_measurement;
    double lambda_c;

    vector<Vector3d, aligned_allocator<Vector3d> > features;
    boost::mt19937 rng;
};

#endif /* ENVIRONMENTSIM_H_ */
