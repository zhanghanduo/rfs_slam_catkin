/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 20, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 *    Details: Probability Hypotheses Density (PHD) filter implementation.
 */

#ifndef FILTERPHD_H_
#define FILTERPHD_H_

#include <rfs_slam/FilterBase.h>
#include <boost/foreach.hpp>
#include <boost/range/combine.hpp>

/** \brief PHD filter implementation*/
template<class observation, class covariance, class robot_state>
class FilterPHD: public FilterBase<observation, covariance, robot_state> {
public:
    FilterBase<observation, covariance, robot_state>::update;
    FilterBase<observation, covariance, robot_state>::predict;
    typedef FilterBase<observation, covariance, robot_state> FB;
    typedef FilterPHD<observation, covariance, robot_state> FPHD;
    typedef typename FB::gmm gmm;
    typedef typename FB::gauss gauss;
    typedef typename FB::vec_observation vec_observation;
    typedef typename FB::vec_covariance vec_covariance;
    typedef typename FB::vec_robot_state vec_robot_state;

    typedef typename FB::vec_vec_observation vec_vec_observation;
    typedef typename FB::vec_vec_robot_state vec_vec_robot_state;
private:
    /** \brief Struct which hold all variables need for extended kalman filter */
    struct Ekf {
        observation mu; //which feature generate observation
        double w; // weight of original feature
        double new_w; //new weight of the updated feature
        observation z; //predicted observation
        covariance S;
        covariance P;
        covariance K;
    };
public:

    /** max_gauss is used to initialize capacity of the GMM vector,
     * Note that for each measurement there are size_of(FOV(features)) gaussians,
     * so assuming there is 20 features in FOV and there is 50 measurements so 50*20=1000
     * gaussian will be created. */
    FilterPHD(unsigned int max_gauss = 4096) {
        FB::initialize();
        this->phd = gmm(max_gauss);
        Dmin = pow(10, -10);
        Tmin = pow(10, -10);
        JMax = 2048;
    }

    FilterPHD(const FPHD& filter) {
        FB::initialize(filter);
        Dmin = filter.Dmin;
        Tmin = filter.Tmin;
        JMax = filter.JMax;
    }

    virtual ~FilterPHD() {
    }

    /** \brief Prune based on local variables */
    virtual void prune(double Dmin = 0, double Tmin = 0, unsigned int Jmax =
            10000) {
        FB::prune(this->Dmin, this->Tmin, this->JMax);
    }

    virtual void update(const vec_vec_observation& vec_observations,
            const vec_vec_robot_state& vec_rss) {
        BOOST_ASSERT(vec_observations.size() != vec_rss.size());
        for (unsigned int robot_id = 0; robot_id < vec_rss.size(); ++robot_id) {
            BOOST_ASSERT(vec_rss[robot_id].empty());
            const vec_robot_state rss = vec_rss[robot_id];

            std::vector<Ekf> ekfs = compute_update_terms(this->phd, rss,
                    robot_id);
            BOOST_FOREACH(gauss& g, this->phd.get_gaussians()) { //Update GMM components
                g.set_w(g.get_w() * (1 - FB::pd(g.get_mu(), rss, robot_id)));
            }
            BOOST_FOREACH(const observation& z, vec_observations[robot_id]) {
                double sum_weights = 0;
                BOOST_FOREACH(Ekf& ekf, ekfs) { //compute weight for each ekf
                    double fpd = ekf.w * FB::pd(ekf.mu, rss, robot_id, 1); //we know it is visible
                    ekf.new_w = fpd * gauss::GaussianPdf(z, ekf.z, ekf.S);
                    sum_weights += ekf.new_w;
                }
                BOOST_FOREACH(const Ekf& ekf, ekfs) {
                    if (sum_weights < std::numeric_limits<double>::epsilon())
                        break;
                    double w = ekf.new_w
                            / (FB::c(z, rss[0], robot_id) + sum_weights);
                    if (w > Tmin) {
                        this->phd.add_gaussian(ekf.mu + ekf.K * (z - ekf.z),
                                ekf.P, w);
                    }
                }
            }
        }
    }

    virtual void predict(const vec_vec_observation& vec_observations,
            const vec_vec_robot_state& vec_rss) {
        BOOST_ASSERT(vec_observations.size() != vec_rss.size());
        for (unsigned int robot_id = 0; robot_id < vec_rss.size(); ++robot_id) {
            BOOST_ASSERT(vec_rss[robot_id].empty());
            BOOST_FOREACH(const observation& z, vec_observations[robot_id]) {
                const robot_state rs = vec_rss[robot_id][0];
                observation mu = FB::h_inverse(z, rs);
                covariance jac = FB::H(mu, rs);
                covariance cov = jac * FB::R(robot_id) * jac.transpose();
                this->phd.add_gaussian(mu, cov, 0.01);
            }
        }
    }

    virtual FPHD * clone(void) const {
        return new FilterPHD(*this);
    }

private:
    /** \brief Propagate states based on extended kalman filter, only visible features are propagated */
    std::vector<Ekf> compute_update_terms(const gmm& phd,
            const vec_robot_state& rss, unsigned int robot_id) {
        std::vector<Ekf> ekfs;
        ekfs.reserve(phd.size()); //assume worst case for reserving
        BOOST_FOREACH(const gauss& g, phd.get_gaussians()) {
           if (!FB::is_in_fov(g.get_mu(), rss, robot_id)) {
                continue;
            }
            Ekf ekf;
            ekf.mu = g.get_mu();
            ekf.w = g.get_w();
            covariance jacobian = FB::H(ekf.mu, rss[0]);
            ekf.z = FB::h(ekf.mu, rss[0]);
            ekf.S = jacobian * g.get_cov() * jacobian.transpose()
                    + FB::R(robot_id);
            ekf.K = g.get_cov() * jacobian.transpose() * ekf.S.inverse();
            ekf.P = (covariance::Identity() - ekf.K * jacobian) * g.get_cov();
            ekfs.push_back(ekf);
        }
        return ekfs;
    }

public:
    double Tmin, Dmin; //Prune elements
    unsigned int JMax; //Prune elements
};

#endif