/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 27, 2013
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Example of using FilterPHD class for 2d features
 */

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <rfs_slam/FilterPHD.h>
#include <rfs_slam/EnvironmentSim.h>
#include <rfs_slam/ParticleFilter.h>
#include <rfs_slam/MotionModel.h>

using namespace Eigen;
typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

class Particle {
public:
    Particle() {
        f.JMax = 500;
        f.Dmin = 0.5;
        f.Tmin = .00001;
        f.CK = 0.05;
        f.PD = 0.95;
        f.R_default = Matrix2d::Identity() * 0.05;
        f.fov_H_angle = M_PI;
        f.fov_V_angle = M_PI;
        f.fov_distance = 4.0;
    }

    Particle(const Particle& p) {
        f = FPHD(p.f);
        rss = FPHD::vec_robot_state(p.rss.begin(), p.rss.end());
        mm = p.mm;
    }

    virtual ~Particle() {
    }

    static double compute_weight(Particle& p,
                                 const FPHD::vec_vec_observation& vec_obsvs) {
        double w = 0;
        for (unsigned int i = 0; i < p.rss.size(); ++i) {
            BOOST_FOREACH(const Vector2d& z, vec_obsvs[i]) {
                w += p.f.phd.pdf(p.f.h_inverse(z, p.rss[i]));
            }
        }
        return w;
    }

    static double compute_weight_gmm_dist(Particle& p, const FPHD::vec_vec_observation& vec_obsvs) {
        FPHD::gmm gmm(1024);//construct GMM from vec_obsvs
        for (unsigned int i = 0; i < p.rss.size(); ++i) {
            BOOST_FOREACH(const Vector2d& z, vec_obsvs[i]) {
                gmm.add_gaussian(p.f.h_inverse(z, p.rss[i]), p.f.R(), 1.0);
            }
        }
        
        FPHD::gmm gmm_fov(p.f.phd.size());
        BOOST_FOREACH(const FPHD::gauss& g, p.f.phd.get_gaussians()) {
            if(p.f.is_in_fov(g.get_mu(), p.rss) && g.get_w() > 0.5) {
                gmm_fov.add_gaussian(g);
            }
        }
        double dist = gmm.distance_to_gmm(gmm_fov, 0.2, 0);
        return dist;
    }

    void motion_predict_update_prune(const FPHD::vec_robot_state& us,
                                     const FPHD::vec_vec_observation& vec_obsvs) {
        motion_model(us);
        predict_update_prune(vec_obsvs);
    }

    void predict_update_prune(const FPHD::vec_vec_observation& vec_obsvs) {
        FPHD::vec_vec_robot_state vec_rss;
        for (unsigned int i = 0; i < rss.size(); ++i) {
            FPHD::vec_robot_state states;
            states.push_back(rss[i]);
            vec_rss.push_back(states);
        }
        f.predict(vec_obsvs, vec_rss);
        f.update(vec_obsvs, vec_rss);
        f.prune();
    }

    void motion_model(const FPHD::vec_robot_state& us) {
        for (unsigned int i = 0; i < us.size(); ++i) {
            rss[i] = mm.motion_model_2d(rss[i], us[i]);
        }
    }

public:
    FPHD f;
    FPHD::vec_robot_state rss;
private:
    MotionModel mm;
};

boost::shared_ptr<EnvironmentSim> env;

void publish_phd(const FPHD::FB& filter);
void publish_transform(const Affine2d& rs, const std::string& frame);
void get_obsvs_and_rss(const FPHD::vec_vec_robot_state& trajectories, FPHD::vec_vec_observation& vec_obsvs,
                       FPHD::vec_vec_robot_state& vec_rss, unsigned int i);
void get_obsvs_and_us(const FPHD::vec_vec_robot_state& trajectories, FPHD::vec_vec_observation& vec_obsvs,
                      FPHD::vec_robot_state& us, unsigned int i);
void publish_particles(const std::vector<Particle>& particles, const std::vector<double>& weights, double w_norm);

int main(int argc, char **argv) {
    ros::init(argc, argv, "phd_slam");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);

    Particle p;
    env.reset(new EnvironmentSim(100));
    env->fov_distance = p.f.fov_distance;
    env->fov_H_angle = p.f.fov_H_angle;
    env->fov_V_angle = p.f.fov_V_angle;
    env->fov_distance_min = p.f.fov_distance_min;
    env->noise_measurement = 0.000;

    FPHD::vec_robot_state rss;
    FPHD::vec_vec_robot_state trajectories(2, rss);
    env->generate_circle_trajectory(2.0, Vector2d(5, 0), 100, trajectories[0]);
    env->generate_circle_trajectory(2.0, Vector2d(-5, 0), 100, trajectories[1]);
    p.rss.push_back(trajectories[0][0]);
    p.rss.push_back(trajectories[1][0]);
    ParticleFilter<Particle> pf(1000, p);

    while (true) {
        for (unsigned int i = 0; i < trajectories[0].size(); ++i) {
            FPHD::vec_vec_observation vec_obsvs;
            FPHD::vec_robot_state us;
            get_obsvs_and_us(trajectories, vec_obsvs, us, i);
            BOOST_FOREACH(Particle& p, pf.particles) {
                p.motion_model(us);
            }
            //pf.set_weight_function(boost::bind(&Particle::compute_weight, _1, vec_obsvs));
            pf.set_weight_function(boost::bind(&Particle::compute_weight_gmm_dist, _1, vec_obsvs));   
           // pf.update_and_resample(true);
            //pf.set_weight_function(boost::lambda::constant(1.0)); // motion model
            //pf.update_and_resample();
            
            pf.update_and_resample(true);
            BOOST_FOREACH(Particle& p, pf.particles) {
                p.predict_update_prune(vec_obsvs);
            }
            
            double w_norm = *std::max_element(pf.weights.begin(), pf.weights.end());
            publish_particles(pf.particles, pf.weights, w_norm);
            publish_phd(pf.get_maximum_weight_particle().f);
            if (!ros::ok())
                return 1;
        }
    }
    return 0;
}

void get_obsvs_and_us(const FPHD::vec_vec_robot_state& trajectories,
                      FPHD::vec_vec_observation& vec_obsvs,
                      FPHD::vec_robot_state& us, unsigned int i) {

    static bool first = true;
    for (unsigned int j = 0; j < trajectories.size(); ++j) {
        Affine2d rs = trajectories[j][i];

        publish_transform(rs,
                          "base_link_" + boost::lexical_cast<std::string>(j));
        FPHD::vec_observation obsvs;
        env->get_observation(obsvs, rs);
        vec_obsvs.push_back(obsvs);

        Affine2d rs_last;
        if(i == 0) {
            rs_last = trajectories[j].back();
        } else {
            rs_last = trajectories[j][i-1];
        }
        if(first) {
            rs_last = rs;
        }
        us.push_back(rs_last.inverse() * rs);
    }
    if(first) {
        first = false;
    }
}

void get_obsvs_and_rss(const FPHD::vec_vec_robot_state& trajectories,
                       FPHD::vec_vec_observation& vec_obsvs,
                       FPHD::vec_vec_robot_state& vec_rss, unsigned int i) {
    for (unsigned int j = 0; j < trajectories.size(); ++j) {
        Affine2d rs = trajectories[j][i];

        publish_transform(rs,
                          "base_link_" + boost::lexical_cast<std::string>(j));
        FPHD::vec_observation obsvs;
        env->get_observation(obsvs, rs);
        FPHD::vec_robot_state rss(1, rs);
        vec_obsvs.push_back(obsvs);
        vec_rss.push_back(rss);
    }
}

void publish_particles(const std::vector<Particle>& particles, const std::vector<double>& weights, double w_norm) {
    static ros::NodeHandle node;
    static ros::Publisher pub = node.advertise<geometry_msgs::PoseArray>("/particles", 1);
    if(pub.getNumSubscribers() == 0)
        return;

    geometry_msgs::PoseArray poses;
    poses.header.frame_id = "/world";

    unsigned int i = 0; //todo make boost_pair
    BOOST_FOREACH(const Particle& p, particles) {
        geometry_msgs::Pose pose;
        BOOST_FOREACH(const Eigen::Affine2d& rs, p.rss) {
            pose.position.x = rs.translation()(0);
            pose.position.y = rs.translation()(1);
            pose.position.z = weights[i] / w_norm;
            Eigen::Rotation2Dd rot(0);
            rot.fromRotationMatrix(rs.rotation());
            pose.orientation = tf::createQuaternionMsgFromYaw(rot.angle());
            poses.poses.push_back(pose);
        }
        ++i;
    }
    pub.publish(poses);
}

void publish_transform(const Affine2d& rs, const std::string& frame) {
    static tf::TransformBroadcaster br;
    tf::StampedTransform stamped_transform;
    stamped_transform.frame_id_ = "world";
    stamped_transform.child_frame_id_ = frame;
    stamped_transform.stamp_ = ros::Time::now();
    Affine3d e;
    e.setIdentity();
    Vector3d v(0, 0, 0);
    v.head(2) = rs.translation();
    Matrix3d rot = Matrix3d::Identity();
    rot.topLeftCorner(2, 2) = rs.rotation();
    e.translate(v).rotate(rot);
    tf::transformEigenToTF(e, stamped_transform);
    br.sendTransform(stamped_transform);
}

void publish_phd(const FPHD::FB& filter) {
    static ros::NodeHandle node;
    static ros::Publisher pub_phd(node.advertise<pcl::PointCloud<pcl::PointXYZI> >("/phd",1));
    if (pub_phd.getNumSubscribers() == 0)
        return;
    float voxel_size = 0.05;
    pcl::PointCloud<pcl::PointXYZI>::Ptr pc(
        new pcl::PointCloud<pcl::PointXYZI>);

    BOOST_FOREACH(const FPHD::gauss& g, filter.phd.get_gaussians()) {
        std::vector<double> eigs;
        FPHD::vec_observation eigvs;
        g.eigen_value_vectors(eigs, eigvs);
        double length = *std::max_element(eigs.begin(), eigs.end());
        length = sqrt(length) * 2;
        if (length > 1.0) {
            ROS_WARN_STREAM("Eigen value 2*sqrt() too large: " << length);
            length = 1.0;
        }
        if (length < 0.05) {
            ROS_WARN_STREAM("Eigen value 2*sqrt() too small: " << length);
            length = 0.1;
        }
        for (float xx = -length; xx <= length; xx += voxel_size) {
            for (float yy = -length; yy <= length; yy += voxel_size) {
                Vector2d state = g.get_mu();
                state(0) += xx;
                state(1) += yy;
                pcl::PointXYZI p;
                p.x = state(0);
                p.y = state(1);
                p.z = 0.0;
                p.intensity = filter.phd.pdf(state);
                //p.z += (p.intensity);
                pc->push_back(p);
            }
        }
    }

    std_msgs::Header header;
    header.frame_id = "world";
    header.stamp = ros::Time::now();
    pc->header = pcl_conversions::toPCL(header);

    pcl::VoxelGrid<pcl::PointXYZI> sor;
    sor.setInputCloud(pc);
    sor.setLeafSize(voxel_size, voxel_size, voxel_size);

    pcl::PointCloud<pcl::PointXYZI> outpc;
    sor.filter(outpc);
    pub_phd.publish(outpc);

}


