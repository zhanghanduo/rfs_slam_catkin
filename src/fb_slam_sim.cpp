/**
 * Copyright (c) CVUT  - All Rights Reserved
 * Created on: 02/16/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Forward-Backward SLAM simulation for two robots
 */

#include <ros/ros.h>
#include <rfs_slam/RosUtils.h>
#include <rfs_slam/FilterPHD.h>
#include <rfs_slam/EnvironmentSim.h>
#include <rfs_slam/ParticleFilter.h>
#include <rfs_slam/MotionModel.h>

using namespace Eigen;
typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

class Particle {
public:
    Particle() {
        f.JMax = 500;
        f.Dmin = 0.5;
        f.Tmin = .001;
        f.CK = 0.05;
        f.PD = 0.95;
        f.R_default = Matrix2d::Identity() * 0.05;
        f.fov_H_angle = M_PI_2;
        f.fov_V_angle = M_PI_2;
        f.fov_distance = 3.0;
        mm.a[0] = 0.005; // rad/rad
        mm.a[1] = 2 * (M_PI / 180.0); // rad/meter
        mm.a[2] = 0.02;// meters/meter
        mm.a[3] = 0.01; // meter/rad
    }

    Particle(const Particle& p) {
        f = FPHD(p.f);
        rss = p.rss;
        mm = p.mm;
    }

    virtual ~Particle() {}

    void predict_update_prune(const FPHD::vec_vec_observation& vec_obsvs) {
        if(rss.size() < vec_obsvs.size()) {
            ROS_WARN_STREAM("All observations cannot be incorporated because particle have not enough poses");
            return;
        }
        for(unsigned int i = 0; i < vec_obsvs.size(); ++i) {
            f.predict(vec_obsvs[i], rss[i]);
            f.update(vec_obsvs[i], rss[i]);
            f.prune();
        }
    }

    void motion(const FPHD::vec_robot_state& us) {
        if(rss.size() < us.size()) {
            ROS_WARN_STREAM("All acions cannot be incorporated because particle have not enough poses");
            return;
        }
        for(unsigned int i = 0; i < us.size(); ++i) {
            rss[i] = mm.motion_model_2d(rss[i], us[i]);
        }
    }

    static double compute_weight_gmm_dist(Particle& p, const FPHD::vec_vec_observation& vec_obsvs) {
        if(p.rss.size() < vec_obsvs.size()) {
            ROS_WARN_STREAM("Cannot compute gmm distance from observations (check size)");
            return 0.0;
        }
        FPHD::gmm gmm(1024);//construct GMM from vec_obsvs
        for(unsigned int i = 0; i < vec_obsvs.size(); ++i) {
            BOOST_FOREACH(const Vector2d & z, vec_obsvs[i]) {
                gmm.add_gaussian(p.f.h_inverse(z, p.rss[i]), p.f.R(), 1.0);
            }
        }

        FPHD::vec_robot_state active_states(p.rss.begin(), p.rss.begin() + vec_obsvs.size());
        FPHD::gmm gmm_fov(p.f.phd.size());
        BOOST_FOREACH(const FPHD::gauss & g, p.f.phd.get_gaussians()) {
            if(p.f.is_in_fov(g.get_mu(), active_states) && g.get_w() > 0.5) {
                gmm_fov.add_gaussian(g);
            }
        }
        double dist = gmm.distance_to_gmm(gmm_fov, 0.2, 0);
        return dist;
    }

public:
    FPHD f;
    FPHD::vec_robot_state rss;
    MotionModel mm;
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "fb_slam_sim");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    Particle p;
    boost::shared_ptr<EnvironmentSim> env;
    env.reset(new EnvironmentSim(30, Vector3d(-4.0, -4.0, 0), Vector3d(4.0, 4.0, 0.0)));
    env->fov_distance = p.f.fov_distance;
    env->fov_H_angle = p.f.fov_H_angle;
    env->fov_V_angle = p.f.fov_V_angle;
    env->fov_distance_min = p.f.fov_distance_min;
    env->noise_measurement = 0.000;
    env->save_features("/tmp/featuers.csv");

    FPHD::vec_robot_state last_rss;
    FPHD::vec_vec_robot_state trajectories(2, last_rss);
    for(int i = 0; i < 3; ++i) {
        const double ang_off_1(M_PI + M_PI / 8), ang_off_2(M_PI / 8);
        env->generate_circle_trajectory(1.75, Vector2d(2.0, 0), 50, trajectories[0], ang_off_1);
        env->generate_circle_trajectory(1.75, Vector2d(-2.0, 0), 50, trajectories[1], ang_off_2);
    }

    //Extract all actions and observations - what robot received for each time
    typedef std::pair<FPHD::vec_robot_state, FPHD::vec_vec_observation> RobotsInput;
    std::vector<RobotsInput> robots_intputs;
    for(unsigned int i = 1; i < trajectories[0].size(); ++i) {
        RobotsInput ri;
        BOOST_FOREACH(FPHD::vec_robot_state & traj, trajectories) {
            ri.first.push_back(traj[i - 1].inverse() * traj[i]);
            FPHD::vec_observation obsv;
            env->get_observation(obsv, traj[i]);
            ri.second.push_back(obsv);
        }
        robots_intputs.push_back(ri);
    }

    int meeting_time = 47;
    Affine2d meeting_transform = trajectories[0][meeting_time].inverse() * trajectories[1][meeting_time];

    p.rss.push_back(trajectories[0][0]); //TODO: maybe should be initialized to Identity to be realistic
    ParticleFilter<Particle> pf(500, p);

    for(unsigned int t = 0; t < robots_intputs.size(); ++t) {
        RobotsInput ri = robots_intputs[t];
        if(t < meeting_time) { //only the first robot observations were received
            ri.first.erase(ri.first.begin() + 1);
            ri.second.erase(ri.second.begin() + 1);
        } else if(t == meeting_time) { //add second and second-virtual robot state
            BOOST_FOREACH(Particle & p, pf.particles) {
               p.rss.push_back(p.rss[0] * meeting_transform);
               p.rss.push_back(p.rss[0] * meeting_transform);
            }
            ROS_INFO_STREAM("Robots meet each other.");
            ROS_INFO_STREAM("  Time:" << t);
            ROS_INFO_STREAM("  Transofrmation:" << meeting_transform.matrix());
        }
        if(t >= meeting_time) {
            static int backward_time = t-1;
            if(backward_time == 0) { //no other observations for virtual robot, can be removed
                BOOST_FOREACH(Particle & p, pf.particles) {
                    p.rss.pop_back();
                }
            } else if(backward_time > 0) {
                Affine2d u = trajectories[1][backward_time+1].inverse() * trajectories[1][backward_time];
                ri.first.push_back(robots_intputs[backward_time].first[1].inverse());
                ri.second.push_back(robots_intputs[backward_time - 1].second[1]); //duplicate second robot
            }
            if(backward_time >= 0)
                backward_time--;
        }
        BOOST_FOREACH(Particle & p, pf.particles) {
            p.motion(ri.first);
        }
        pf.set_weight_function(boost::bind(&Particle::compute_weight_gmm_dist, _1, ri.second));
        pf.update_and_resample(true);

        BOOST_FOREACH(Particle & p, pf.particles) {
            p.predict_update_prune(ri.second);
        }

        for(unsigned int index = 0; index < pf.get_maximum_weight_particle().rss.size(); ++index) {
            std::stringstream ss;
            ss << "base_link_" << index;
            ROSUtils::publish_transform(pf.get_maximum_weight_particle().rss[index], ss.str());
            ROSUtils::publish_vision(env->fov_distance_min, env->fov_distance, env->fov_H_angle, ss.str());
        }
        FPHD::vec_robot_state poses;
        BOOST_FOREACH(const Particle & p, pf.particles) {
            BOOST_FOREACH(const Affine2d & rs, p.rss) {
                poses.push_back(rs);
            }
        }
        ROSUtils::publish_phd(pf.get_maximum_weight_particle().f);
        ROSUtils::publish_poses(poses, pf.weights);
        ROSUtils::save_poses(poses, pf.weights, "/tmp/poses.csv");
        if(!ros::ok())
            return 1;
    }

    spinner.stop();
    return 0;
}
