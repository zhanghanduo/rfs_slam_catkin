/**
 * Copyright (c) Alpaca  - All Rights Reserved
 * Created on: 02/16/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: SilliconHill
 *    Details: Test close loop - in theory the deviation of the particles should lowerize after loop is closed
 */

#include <ros/ros.h>
#include <rfs_slam/RosUtils.h>
#include <rfs_slam/FilterPHD.h>
#include <rfs_slam/EnvironmentSim.h>
#include <rfs_slam/ParticleFilter.h>
#include <rfs_slam/MotionModel.h>

using namespace Eigen;
typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

class Particle {
public:
     Particle() {
        f.JMax = 500;
        f.Dmin = 0.5;
        f.Tmin = .00001;
        f.CK = 0.05;
        f.PD = 0.95;
        f.R_default = Matrix2d::Identity() * 0.05;
        f.fov_H_angle = M_PI_2;
        f.fov_V_angle = M_PI_2;
        f.fov_distance = 1.0;
        mm.a[0] = 0.005; // rad/rad
        mm.a[1] = 2 * (M_PI / 180.0); // rad/meter
        mm.a[2] = 0.02;// meters/meter
        mm.a[3] = 0.01; // meter/rad
    }

    Particle(const Particle& p) {
        f = FPHD(p.f);
        rs = p.rs;
        mm = p.mm;
    }

    virtual ~Particle() {}
    
    void motion(const Affine2d& u) {
        rs = mm.motion_model_2d(rs, u);
    }
    
    void predict_update_prune(const FPHD::vec_observation& vec_obsv) {
        f.predict(vec_obsv, rs);
        f.update(vec_obsv, rs);
        f.prune();
    }
    
    static double compute_weight_gmm_dist(Particle& p, const FPHD::vec_observation& vec_obsv) {
        FPHD::gmm gmm(1024);//construct GMM from vec_obsv
        BOOST_FOREACH(const Vector2d& z, vec_obsv) {
            gmm.add_gaussian(p.f.h_inverse(z, p.rs), p.f.R(), 1.0);
        }
        
        FPHD::gmm gmm_fov(p.f.phd.size());
        BOOST_FOREACH(const FPHD::gauss& g, p.f.phd.get_gaussians()) {
            if(p.f.is_in_fov(g.get_mu(), p.rs) && g.get_w() > 0.5) {
                gmm_fov.add_gaussian(g);
            }
        }
        double dist = gmm.distance_to_gmm(gmm_fov, 0.2, 0);
        return dist;
    }
    
public:
    FPHD f;
    Affine2d rs;
    MotionModel mm;
};

int main(int argc, char **argv) {
    ros::init(argc, argv, "close_loop");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    
    Particle p;
    boost::shared_ptr<EnvironmentSim> env;
    env.reset(new EnvironmentSim(250));
    env->fov_distance = p.f.fov_distance;
    env->fov_H_angle = p.f.fov_H_angle;
    env->fov_V_angle = p.f.fov_V_angle;
    env->fov_distance_min = p.f.fov_distance_min;
    env->noise_measurement = 0.000;
    env->save_features("/tmp/featuers.csv");

    FPHD::vec_robot_state trajectory;
    env->generate_circle_trajectory(3.0, Vector2d(0,0), 50, trajectory);
    p.rs = trajectory[0];
    ParticleFilter<Particle> pf(500, p);
    
    Affine2d last_rs = trajectory[0];
    while(ros::ok()) {
        BOOST_FOREACH(const Affine2d& rs, trajectory) {
            Affine2d u = last_rs.inverse() * rs;
            last_rs = rs;
            BOOST_FOREACH(Particle& p, pf.particles) {
                p.motion(u);
            }
            FPHD::vec_observation obsv;
            env->get_observation(obsv, rs);
            pf.set_weight_function(boost::bind(&Particle::compute_weight_gmm_dist, _1, obsv));
            pf.update_and_resample(true);
            BOOST_FOREACH(Particle& p, pf.particles) {
                p.predict_update_prune(obsv);
            }
            {
                FPHD::vec_robot_state rss;
                BOOST_FOREACH(const Particle& p, pf.particles) {
                    rss.push_back(p.rs);
                }
                ROSUtils::publish_poses(rss, pf.weights);
                ROSUtils::save_poses(rss, pf.weights, "/tmp/poses.csv");
                ROSUtils::publish_phd(pf.get_maximum_weight_particle().f);
                ROSUtils::publish_transform(pf.get_maximum_weight_particle().rs, "/base_link_1");
                ROSUtils::publish_vision(env->fov_distance_min, env->fov_distance, env->fov_H_angle, "/base_link_1");
            }
            if (!ros::ok())
                return 1;
        }
    }
    
    ros::waitForShutdown();
    spinner.stop();
    return 0;
}